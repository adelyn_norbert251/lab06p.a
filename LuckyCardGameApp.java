import java.util.Scanner;
import java.util.Random;
public class LuckyCardGameApp{
	public static void main(String[]args){
		Scanner reader = new Scanner(System.in);
		Deck pack = new Deck();
		pack.shuffle();
		System.out.println("Hi ! How many cards do you wish to remove ?");
		int answer = reader.nextInt();
		reader.nextLine();
		System.out.println("Number of cards: " + pack.lengthOfDeck());
		System.out.println("Cards left: " + (pack.lengthOfDeck()-answer));
		pack.shuffle();
		System.out.println(pack.toString());
	}
}
